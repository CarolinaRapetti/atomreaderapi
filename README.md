# README #

API de recepción y parseo de noticias de tipo Atom realizada por el Grupo 4.

### Como se usa? ###
1. Obtener una instancia de NewsAPI

2. Con esta instancia se le puede requerir un atomNewsFetcher con el método getAtomNewsFetcher(), este será el objeto que recibirá urls y obtendrá las noticias.

3. Cargar 1 o más urls atom validas a atomNewsFetcher con el método addUrl(url)

4. Crear una Lista de AtomFeed para obtener la lista del atomNewsFetcher con el método getNews(). NOTA: Este método arrojará una excepción si no se agregaron URLs.

5. Los AtomFeeds tienen 2 métodos útiles: getTitle() que retornará el título de la noticia y getLink() que retornará el link.

```
#!java

public void getNews() {
//Se obtiene una instancia del objeto NewsAPI
		NewsAPI api = NewsAPI.getInstance();
//Con esa instancia se obtiene un objeto atomNewsFetcher
		AtomNewsFetcher fetcher = api.getAtomNewsFetcher();
//Se le carga una url
		fetcher.addUrl("http://www.theregister.co.uk/science/headlines.atom");
		//Se obtendra una lista de AtomFeeds
		List<AtomFeed> feeds = new LinkedList<AtomFeed>();
		try {
			feeds = fetcher.getNews();
//Con los feeds obtenidos Se imprimen los valores de cada uno
			for( AtomFeed af: feeds){
				System.out.println("Titulo: "+af.getTitle());
				System.out.println("Link: "+af.getLink());
				System.out.println("-----");
			}
		} catch (NoURLSetException e) {
//Se decide que hacer ante la excepcion de no haber agregado URL
			e.printStackTrace();
		}
	}
```