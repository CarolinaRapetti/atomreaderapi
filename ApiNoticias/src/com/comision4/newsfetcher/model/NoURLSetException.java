package com.comision4.newsfetcher.model;

/**
 * Class NoURLSetException
 * 
 * Exception que ocurre cuando se piden las noticias cuando no se paso ninguna URL a las clases AtomNewsFetcher y NewsAtomReceiver
 *
 * @author Cangelosi Juan Ignacio; Rapetti Carolina; Piersigilli Joaqu�n.
 * 
 */
@SuppressWarnings("serial")
public class NoURLSetException extends Exception {

}
