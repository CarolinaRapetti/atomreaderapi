package com.comision4.newsfetcher.logic;

import java.util.LinkedList;
import java.util.List;

import com.comision4.newsfetcher.model.AtomFeed;
import com.comision4.newsfetcher.model.NoURLSetException;

class AtomNewsFetcherImp implements AtomNewsFetcher{
	private List<String> urls = new LinkedList<String> ();
	
	@Override
	public void addUrl(String url) {
		urls.add(url);
	}

	@Override
	public List<AtomFeed> getNews() throws NoURLSetException{
		if(urls.size() == 0){
			throw new NoURLSetException();
		}
		
		List<AtomFeed> salida = new LinkedList<AtomFeed>();
		NewsAtomReceiver fetcher = new NewsAtomReceiver();
		
		try{
			for (String s : urls){
				fetcher.setURL(s);
				AtomFeed[] feed = fetcher.getNews();
				for( int i= 0; i< feed.length; i++){
					salida.add(feed[i]);
				}
			}
		} catch (NoURLSetException e){
			e.printStackTrace();
		}
		
		return salida;
	}
		
}
