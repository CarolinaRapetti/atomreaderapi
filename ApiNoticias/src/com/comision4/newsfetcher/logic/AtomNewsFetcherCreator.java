package com.comision4.newsfetcher.logic;
/**
 * Class AtomNewsFetcherCreator
 * 
 * Esta clase permite la creacion de AtomNewsFetchers ya que por visibilidad de paquetes el padre no puede ver las clases
 * que tienen visibilidad restringida en un paquete hijo.
 *
 * @author Cangelosi Juan Ignacio; Rapetti Carolina; Piersigilli Joaqu�n.
 * 
 */
public class AtomNewsFetcherCreator {
	private static AtomNewsFetcherCreator instance;
	
	private AtomNewsFetcherCreator(){
		
	}
	
	public static AtomNewsFetcherCreator getInstance(){
		if(instance==null){
			instance= new AtomNewsFetcherCreator();
		}
		return instance;
	}

	public AtomNewsFetcher createAtomFetcher(){
		return new AtomNewsFetcherImp();
	}
}
