package com.comision4.newsfetcher.logic;

import java.util.List;

import com.comision4.newsfetcher.model.AtomFeed;
import com.comision4.newsfetcher.model.NoURLSetException;
/**
 * Interface AtomNewsFetcher 
 * 
 * Esta es la interfaz p�blica de las clases que obtendran multiples urls y obtendr�n y parsearan las noticias de esas urls
 *
 * @author Cangelosi Juan Ignacio; Rapetti Carolina; Piersigilli Joaqu�n.
 * 
 */
public interface AtomNewsFetcher {
	public void addUrl(String url);
	
	public List<AtomFeed> getNews() throws NoURLSetException;
	
	
}
