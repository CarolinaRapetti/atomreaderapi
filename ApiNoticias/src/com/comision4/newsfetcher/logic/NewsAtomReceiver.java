package com.comision4.newsfetcher.logic;

/**
 * Class NewsAtomModel
 * 
 * Esta clase implementa la interfaz NewsModel.
 * Implementa la l�gica de obtener noticias en formato XML Atom de una url espec�fica.
 *
 * @author Cangelosi Juan Ignacio; Rapetti Carolina; Piersigilli Joaqu�n.
 * 
 */

import java.io.IOException;
import java.net.URL;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.comision4.newsfetcher.model.AtomFeed;
import com.comision4.newsfetcher.model.NoURLSetException;
/**
 * Class NewsAtomReceiver
 * 
 * Esta clase recive una URL y devuelve las noticias de esa pagina.
 *
 * @author Cangelosi Juan Ignacio; Rapetti Carolina; Piersigilli Joaqu�n.
 * 
 */
public class NewsAtomReceiver {
	private String atom_url;

	public AtomFeed[] getNews() throws NoURLSetException{
		//Se inicializa en 0 para evitar el error de compilacion de variable no inicializada
		AtomFeed[] news= new AtomFeed[0];
		
		if(atom_url == null){
			throw new NoURLSetException();
		}
		
		try {
			URL url = new URL(atom_url);
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder db = null;


			db = dbf.newDocumentBuilder();

			Document doc = db.parse(url.openStream());
			NodeList items = doc.getDocumentElement()
					.getElementsByTagName("entry");

			news = new AtomFeed[items.getLength()];
			for(int i = 0; i < items.getLength(); i++){
				AtomFeed f;
				Element item = (Element)items.item(i);
				Element title =  (Element)item.getElementsByTagName("title").item(0);
				Element link =  (Element)item.getElementsByTagName("link").item(0);

				f = new AtomFeed (title.getTextContent(),link.getAttribute("href"));
				news[i]=f;

			}
		} catch (SAXException e) {
			// handle SAXException
			e.printStackTrace();
		} catch (IOException e) {
			// handle IOException
			e.printStackTrace();

		} catch (ParserConfigurationException e1) {
			// handle ParserConfigurationException
			e1.printStackTrace();
		}
		
		
		return news;
	}
	
	public void setURL(String url) {
		atom_url=url;
	}

}
