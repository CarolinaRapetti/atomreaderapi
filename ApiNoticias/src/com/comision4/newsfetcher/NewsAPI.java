package com.comision4.newsfetcher;

import com.comision4.newsfetcher.logic.AtomNewsFetcher;
import com.comision4.newsfetcher.logic.AtomNewsFetcherCreator;
/**
 * Class NewsAPI
 * 
 * Esta clase es el punto de entrada a la API. Le pide al singleton de Logic que cree un AtomFetcher.
 *
 * @author Cangelosi Juan Ignacio; Rapetti Carolina; Piersigilli Joaqu�n.
 * 
 */
public class NewsAPI {
	private static NewsAPI instance;
	
	private NewsAPI(){
		
	}
	
	public static NewsAPI getInstance(){
		if(instance==null){
			instance= new NewsAPI();
		}
		return instance;
	}
	
	public AtomNewsFetcher getAtomNewsFetcher(){
		return AtomNewsFetcherCreator.getInstance().createAtomFetcher();
	}
}
